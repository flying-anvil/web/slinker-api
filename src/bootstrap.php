<?php

use FlyingAnvil\Slinker\ContainerFactory;
use FlyingAnvil\Slinker\Slim\SlimAppFactory;

require_once __DIR__ . '/../vendor/autoload.php';

(static function () {
    chdir(dirname(__DIR__));

    $container = ContainerFactory::create();
    $app = SlimAppFactory::create($container);
    $app->run();
})();

