<?php

declare(strict_types=1);

namespace FlyingAnvil\Slinker;

use DI\Container;
use DI\ContainerBuilder;
use FlyingAnvil\Libfa\DataObject\Application\AppEnv;
use FlyingAnvil\Libfa\Repository\EnvironmentRepository;
use FlyingAnvil\Slinker\Client\Factory\PdoFactory;
use FlyingAnvil\Slinker\Logger\Factory\LoggerFactory;
use FlyingAnvil\Slinker\Options\Factory\LoggingOptionsFactory;
use FlyingAnvil\Slinker\Options\LoggingOptions;
use Monolog\Logger;
use PDO;
use Psr\Log\LoggerInterface;

use function DI\env;
use function DI\factory;
use function DI\get;

final class ContainerFactory
{
    public static function create(): Container
    {
        $envRepo = new EnvironmentRepository();

        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions([
            AppEnv::class          => AppEnv::createFromEnvironmentVariable(),
            Logger::class          => factory(LoggerFactory::class),
            LoggerInterface::class => get(Logger::class),

            PDO::class => factory(PdoFactory::class),

            LoggingOptions::class => factory(LoggingOptionsFactory::class),

            'loggingOptions' => [
                'logLevel'     => env('LOG_LEVEL', 'INFO'),
                'logDirectory' => env('LOG_DIRECTORY'),
            ],
        ]);

        return $containerBuilder->build();
    }
}
