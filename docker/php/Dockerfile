FROM composer:2.1.5 as composer

# ===== BASE =====================================
FROM php:8.0.9-fpm-buster as base

WORKDIR /app/

RUN chown -R www-data:www-data /app/

RUN apt update \
 && apt install -y \
     git unzip \
     libzip-dev \
     libc-client-dev \
 && apt clean \
 && docker-php-ext-install -j$(nproc) zip opcache pdo pdo_mysql

COPY ./docker/php/php-custom.ini /usr/local/etc/php/conf.d/php-custom.ini

# ===== BUILD ====================================
FROM base as build

COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY composer.* /app/

# composer install
RUN chown www-data: /var/www/
USER www-data
RUN composer install --no-progress

USER root

# ===== DEVELOPMENT ==============================
FROM build as development

# Install xdebug
RUN apt update \
 && apt install --no-install-recommends --assume-yes --quiet \
     ca-certificates \
     curl \
     git \
 && apt clean \
 && pecl install xdebug-3.0.4 \
 && docker-php-ext-enable xdebug

COPY ./docker/php/php-xdebug.ini /usr/local/etc/php/conf.d/
EXPOSE 9003

RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

USER 1000:1000

# ===== PRODUCTION ===============================
FROM base as production

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY --from=build /app /app

COPY . /app
